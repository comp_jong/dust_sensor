package com.example.dustmonitor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
/**
 * Created by jeonghun on 2017-11-21.
 */
public class LocationSelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location_selection);
    }
    public void onClick(View v){
        int id = v.getId();
          Intent intentMenuAct = new Intent(LocationSelectionActivity.this, PersonalStaticActivity.class);
          startActivity(intentMenuAct);

    }

}
