package com.example.dustmonitor;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class SettingActivity extends AppCompatActivity {
    SQLiteDatabase db;
    ListView loglist;
    CustomLogAdapter adapt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        loglist = (ListView) findViewById(R.id.custom_query_list);
        loadDB();
    }

    public void loadDB() {

        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        Cursor c = db.rawQuery("SELECT * FROM dust;", null);
        startManagingCursor(c);

        //CustomAdapter 이용. 데이터 add시작
        adapt = new CustomLogAdapter();

        while (c.moveToNext()) {
            adapt.addItem( Integer.parseInt(c.getString(1)), Integer.parseInt(c.getString(2)), Integer.parseInt(c.getString(3)), Integer.parseInt(c.getString(4)),
                    c.getString(5), Double.parseDouble(c.getString(6)));
        }

        /*
        for(int i = 0; i < c.getCount(); i++) {


            adapt.addItem(c.getInt(1), c.getInt(2), c.getInt(3), c.getInt(4), c.getString(5), c.getDouble(6));
        }*/

        loglist.setAdapter(adapt);

        if(db!=null) {
            db.close();
        }
    }

    public void onClickButton(View v) {

        EditText commandEdit = (EditText) findViewById(R.id.custom_command);
        String command = commandEdit.getText().toString();

        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        //db.execSQL("insert into dust ");

        db.execSQL(command);
        updateList();
        //loadDB();
    }


    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    public void updateList() {
        adapt.clearList();
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        Cursor c = db.rawQuery("SELECT * FROM dust;", null);
        startManagingCursor(c);
        while (c.moveToNext()) {
            adapt.addItem( Integer.parseInt(c.getString(1)), Integer.parseInt(c.getString(2)), Integer.parseInt(c.getString(3)), Integer.parseInt(c.getString(4)),
                    c.getString(5), Double.parseDouble(c.getString(6)));
        }
        adapt.notifyDataSetChanged();
    }
}
