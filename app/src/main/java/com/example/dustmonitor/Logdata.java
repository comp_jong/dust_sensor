package com.example.dustmonitor;

/**
 * Created by 하품쟁이 on 2017-11-07.
 */

public class Logdata {

    private int year;
    private int month;
    private int day;
    private int time;
    private String location;
    private double density;

    public Logdata() {    }

    public Logdata(int year, int month, int day, int time, String location, double density) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.time = time;
        this.location = location;
        this.density = density;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setTime (int time) {

        this.time = time;
    }
    public void setLocation (String location) {
        this.location = location;
    }
    public void setDensity (double density) {
        this.density = density;
    }

    public int getYear() {
        return this.year;
    }
    public int getMonth() {
        return this.month;
    }
    public int getDay() {
        return this.day;
    }
    public int getTime() {
        return this.time;
    }
    public String getLocation() {
        return this.location;
    }
    public double getDensity () {
        return  this.density;
    }
}
