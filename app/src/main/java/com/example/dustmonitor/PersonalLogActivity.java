package com.example.dustmonitor;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

public class PersonalLogActivity extends AppCompatActivity {
    ListView loglist;
    CustomLogAdapter adapt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_log);

        loglist = (ListView) findViewById(R.id.personallog_listview);
        loadDB();
    }

    public void loadDB() {
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        Cursor c = db.rawQuery("SELECT * FROM dust;", null);
        startManagingCursor(c);

        //CustomAdapter 이용. 데이터 add시작
        adapt = new CustomLogAdapter();
        while (c.moveToNext()) {
            adapt.addItem( Integer.parseInt(c.getString(1)), Integer.parseInt(c.getString(2)), Integer.parseInt(c.getString(3)), Integer.parseInt(c.getString(4)),
                    c.getString(5), Double.parseDouble(c.getString(6)));
        }

        loglist.setAdapter(adapt);

        if(db!=null) {
            db.close();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateList();
    }

    public void updateList() {
        adapt.clearList();
        SQLiteDatabase db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );
        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        Cursor c = db.rawQuery("SELECT * FROM dust;", null);
        startManagingCursor(c);
        while (c.moveToNext()) {
            adapt.addItem( Integer.parseInt(c.getString(1)), Integer.parseInt(c.getString(2)), Integer.parseInt(c.getString(3)), Integer.parseInt(c.getString(4)),
                    c.getString(5), Double.parseDouble(c.getString(6)));
        }
        adapt.notifyDataSetChanged();
    }
}
