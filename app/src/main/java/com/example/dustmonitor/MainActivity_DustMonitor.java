package com.example.dustmonitor;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import java.io.InputStream;
import java.io.OutputStream;
import android.util.Log;

public class MainActivity_DustMonitor extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //BLE Variable
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private Handler scanHandler = new Handler();
    private boolean isScanning = false;
    private int scan_interval_ms = 2000;
    private RelativeLayout layout;
    private RelativeLayout layout2;
    private ImageView imageview = null;

    private ImageView safeimage = null;
    private ImageView ordiimage = null;
    private ImageView badimage = null;
    private ImageView dangerimage = null;

    String tempstationData[];
    String locationName[];
    ArrayList<String> stationData;

    //SQliteDB Variable
    CustomLogAdapter adapt;

    //Notification Variable
    PendingIntent mPendInt;

    static final int G_NOTIFY_NUM = 1; // 식별ID
    Notification mNoti;
    NotificationManager m_NotiManager;
    ImageView tempImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dust_monitor_main);
        layout =   (RelativeLayout)findViewById(R.id.dustmonitor_relativelayout);
        layout2 =  (RelativeLayout) findViewById(R.id.dustmonitor_relativelayout2);
        imageview = (ImageView)findViewById(R.id.dustmonitor_vectorDust);

        safeimage = (ImageView) findViewById(R.id.dustmonitor_safeimage);
        ordiimage = (ImageView) findViewById(R.id.dustmonitor_ordiimage);
        badimage = (ImageView) findViewById(R.id.dustmonitor_badimage);
        dangerimage = (ImageView) findViewById(R.id.dustmonitor_dangerimage);
        safeimage.setVisibility(View.INVISIBLE);
        ordiimage.setVisibility(View.INVISIBLE);
        badimage.setVisibility(View.INVISIBLE);
        dangerimage.setVisibility(View.INVISIBLE);

        tempstationData = getResources().getStringArray(R.array.locationNumber);
        locationName = getResources().getStringArray(R.array.locationName);
        stationData = new ArrayList<String>(Arrays.asList(tempstationData));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT).show();
 //           finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.

        btManager = (BluetoothManager)getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();

        // Checks if Bluetooth is supported on the device.
        if (btAdapter == null) {
            Toast.makeText(this, R.string.error_bluetooth_not_supported, Toast.LENGTH_SHORT).show();
  //          finish();
            return;
        }

        tempImage = new ImageView(this);

        //InsertData("1","2","3","4","5");

        //Notification 설정
        Intent i = new Intent(this, MainActivity_DustMonitor.class);
     //   i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
     //   i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mPendInt = PendingIntent.getActivity(this, 0, i, 0);
        m_NotiManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
   }

   private static String TAG = "MainActivity_DustMonitor";

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main__dust_monitor, menu);

        if (!isScanning) {
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_stop).setVisible(false);

        } else {
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_stop).setVisible(true);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_scan:
                scanHandler.post(scanRunnable);
                break;
            case R.id.menu_stop:
                scanHandler.post(scanRunnable);
                TextView sensor_density = (TextView) findViewById(R.id.dustmonitor_density);
                TextView sensor_level = (TextView) findViewById(R.id.dustmoniotr_textlevel);
                TextView sensor_location = (TextView) findViewById(R.id.dustmonitor_location);
                sensor_density.setText("스캔을 중지합니다.");
                sensor_level.setText("");
                sensor_location.setText("");
                layout.setBackgroundResource(R.color.soso);
                layout2.setBackgroundResource(R.color.sosounder);
                imageview.setImageResource(R.drawable.soso);

                safeimage.setVisibility(View.INVISIBLE);
                ordiimage.setVisibility(View.INVISIBLE);
                badimage.setVisibility(View.INVISIBLE);
                dangerimage.setVisibility(View.INVISIBLE);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

         if (id == R.id.personal_statics) {
            Intent intentMenuAct = new Intent(MainActivity_DustMonitor.this, PersonalStaticActivity.class);
            startActivity(intentMenuAct);
        } else if (id == R.id.server_statics) {
            Intent intentMenuAct = new Intent(MainActivity_DustMonitor.this, LocationSelectionActivity.class);
            startActivity(intentMenuAct);
        } else if (id == R.id.dust_map) {
             Intent intentMenuAct = new Intent(MainActivity_DustMonitor.this, DustMapActivity.class);
             startActivity(intentMenuAct);
        } else if (id == R.id.peronal_log) {
            Intent intentMenuAct = new Intent(MainActivity_DustMonitor.this, PersonalLogActivity.class);
            startActivity(intentMenuAct);
        } else if (id == R.id.setting) {
            Intent intentMenuAct = new Intent(MainActivity_DustMonitor.this, SettingActivity.class);
            startActivity(intentMenuAct);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //BLE 실시간 통신 부분

    private Runnable scanRunnable = new Runnable()
    {
        @Override
        public void run() {

          //  Log.i("UUID", "run");

            if (isScanning)
            {
                if (btAdapter != null)
                {
                   // Log.i("UUID", "stop");
                    btAdapter.stopLeScan(leScanCallback);
                }
            }
            else
            {
                if (btAdapter != null)
                {
                    //Log.i("UUID", "start");
                    btAdapter.startLeScan(leScanCallback);

                }
            }

            isScanning = !isScanning;
            invalidateOptionsMenu();
           // scanHandler.postDelayed(this, scan_interval_ms);
        }
    };

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback()
    {
        @Override
        public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord)
        {
            Log.i("UUID", "onlescan");
            //   Log.i("UUID", scanRecord.toString() );
            int startByte = 2;
            boolean patternFound = false;
            String location;

            while (startByte <= 5)
            {
                if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                        ((int) scanRecord[startByte + 3] & 0xff) == 0x15)
                { //Identifies correct data length
                    patternFound = true;
                    break;
                }
                startByte++;
            }
            //   Log.i("UUID", "onlescan haha");
            if (patternFound)
            {
                Log.i("UUID", "patternFound");
                //Convert to hex String
                byte[] uuidBytes = new byte[16];

                System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                String hexString = bytesToHex(uuidBytes);
                // Log.i("UUID", "=== " + hexString);

                if(hexString.equals("E2C56DB5DFFB48D2B060D0F5A71096E1")) {
                    //UUID detection
                    String uuid = hexString.substring(0, 8) + "-" +
                            hexString.substring(8, 12) + "-" +
                            hexString.substring(12, 16) + "-" +
                            hexString.substring(16, 20) + "-" +
                            hexString.substring(20, 32);


                    // major
                    final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);

                    // minor
                    final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);

                    //로그
                    //Log.i("DeviceScanActivity", "UUID: " + uuid + "\\nmajor: " + major + "\\nminor" + minor + "MAC :" + device + "RSSI : " + rssi);
                }

                String uuid = hexString.substring(0, 8) + "-" +
                        hexString.substring(8, 12) + "-" +
                        hexString.substring(12, 16) + "-" +
                        hexString.substring(16, 20) + "-" +
                        hexString.substring(20, 32);


                // major
                final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);

                // minor
                final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);

                Log.i("DeviceScanActivity", "UUID: " + uuid + "\\nmajor: " + major + "\\nminor" + minor + "MAC :" + device + "RSSI : " + rssi);
                TextView sensor_density = (TextView) findViewById(R.id.dustmonitor_density);
                TextView sensor_level = (TextView) findViewById(R.id.dustmoniotr_textlevel);
                TextView sensor_location = (TextView) findViewById(R.id.dustmonitor_location);



                float densitytemp = Float.parseFloat( String.format("%d", major) + "." + String.format("%02d", minor) );
               // float densitytemp=280;
                String leveltemp = "";
               //농도 : 에러;
                if ( densitytemp < 0 || densitytemp > 500) {
                    leveltemp = "에러";
                }
                //농도 : 좋음
                else if ( densitytemp > 0 && densitytemp < 50 )
                {
                    leveltemp = "좋음";
                    layout.setBackgroundResource(R.color.good);
                    layout2.setBackgroundResource(R.color.goodunder);
                    imageview.setImageResource(R.drawable.good);

                    safeimage.setVisibility(View.VISIBLE);
                    ordiimage.setVisibility(View.INVISIBLE);
                    badimage.setVisibility(View.INVISIBLE);
                    dangerimage.setVisibility(View.INVISIBLE);
                }

                //농도 : 보통
                else if ( densitytemp >= 50 && densitytemp < 100 ) {
                    leveltemp = "보통";
                    layout.setBackgroundResource(R.color.soso);
                    layout2.setBackgroundResource(R.color.sosounder);
                    imageview.setImageResource(R.drawable.soso);

                    safeimage.setVisibility(View.INVISIBLE);
                    ordiimage.setVisibility(View.VISIBLE);
                    badimage.setVisibility(View.INVISIBLE);
                    dangerimage.setVisibility(View.INVISIBLE);

                }
                //농도 : 나쁨
                else if ( densitytemp >= 100 && densitytemp < 250 ) {
                    leveltemp = "나쁨";
                    layout.setBackgroundResource(R.color.bad);
                    layout2.setBackgroundResource(R.color.badunder);
                    imageview.setImageResource(R.drawable.bad);

                    safeimage.setVisibility(View.INVISIBLE);
                    ordiimage.setVisibility(View.INVISIBLE);
                    badimage.setVisibility(View.VISIBLE);
                    dangerimage.setVisibility(View.INVISIBLE);
                    setAlarm1(densitytemp);
                }
                //농도 : 매우 나쁨
                else if ( densitytemp >= 250 ) {
                    leveltemp = "매우나쁨";
                    layout.setBackgroundResource(R.color.verybad);
                    layout2.setBackgroundResource(R.color.verybadunder);
                    imageview.setImageResource(R.drawable.verybad);

                    safeimage.setVisibility(View.INVISIBLE);
                    ordiimage.setVisibility(View.INVISIBLE);
                    badimage.setVisibility(View.INVISIBLE);
                    dangerimage.setVisibility(View.VISIBLE);
                    setAlarm1(densitytemp);
                }

                //농도 : 에러 값

                sensor_density.setText( "density:" + String.format("%d", major) + "." + String.format("%02d", minor) );
                sensor_level.setText(leveltemp);

                // DB에 데이터 넣는부분

                //DB에 값 저장

                // 현재시간을 msec 으로 구한다.
                long now = System.currentTimeMillis();
                // 현재시간을 date 변수에 저장한다.
                Date date = new Date(now);
                // 시간을 나타냇 포맷을 정한다 ( yyyy/MM/dd 같은 형태로 변형 가능 )
                SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy,MM,dd,HHmm");
                SimpleDateFormat nowYear = new SimpleDateFormat("yyyy");
                SimpleDateFormat nowMonth = new SimpleDateFormat("MM");
                SimpleDateFormat nowDay = new SimpleDateFormat("dd");
                SimpleDateFormat nowTime = new SimpleDateFormat("HHmm");
                // nowDate 변수에 값을 저장한다.
                String formatDate = sdfNow.format(date);

                SQLiteDatabase db = openOrCreateDatabase(
                        "test.db",
                        SQLiteDatabase.CREATE_IF_NECESSARY,
                        null
                );

                db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                        "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

                location = "02052";
                //String dbcommand = String.format("insert into dust(year,month,day,time,location,density) values (%s,'%s',%f);", formatDate, "경북대학교 실습실 창가", densitytemp);
                String dbcommand = String.format("insert into dust(year,month,day,time,location,density) values (%s,%s,%s,%s,'%s',%f);", nowYear.format(date), nowMonth.format(date), nowDay.format(date), nowTime.format(date),
                        location, densitytemp);
                System.out.println(dbcommand);

                Cursor c = db.rawQuery("SELECT * FROM dust WHERE year=" + nowYear.format(date) + " and month=" + nowMonth.format(date) + " and day=" + nowDay.format(date) + " and time=" + nowTime.format(date) +  ";", null);
                //이미 값이 존재하는 row인지?
                if( c.getCount() > 0 ) {
                    if(db!=null) {
                        db.close();
                    }
                    return;
                }

                for(int i=0; i < stationData.size(); i++) {
                    if ( stationData.get(i).equals(location) ) {
                        sensor_location.setText(String.format("%s", locationName[i]));
                        break;
                    }
                }


                db.execSQL(dbcommand);

                if(db!=null) {
                    db.close();
                }
                InsertData task = new InsertData();
                task.execute("1", "02052", nowYear.format(date) + nowMonth.format(date) + nowDay.format(date), nowTime.format(date), Float.toString(densitytemp) );
            }
        }
    };

    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ )
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public void InsertData(String d1, String d2, String d3, String d4, String d5)
    {
        InsertData task = new InsertData();
        task.execute(d1,d2,d3,d4,d5);
    }

    class InsertData extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {

            String id = (String)params[0];
            String location = (String)params[1];
            String yy = (String)params[2];
            String hh = (String)params[3];
            String dust = (String)params[4];



            String serverURL = "http://20.20.3.62/INSERT.php";
            String postParameters = "id=" + id +  "&location=" + location
                    + "&yy=" + yy + "&hh=" + hh
                    + "&dust=" + dust;

            System.out.printf("%s %s %s %s %s", id, location, yy, hh, dust);
            System.out.printf("%s", postParameters);
            System.out.println("hey");
            System.out.printf("%s", postParameters);

            try {

                URL url = new URL(serverURL);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();


                httpURLConnection.setReadTimeout(5000);
                httpURLConnection.setConnectTimeout(5000);
                httpURLConnection.setRequestMethod("POST");
                //httpURLConnection.setRequestProperty("content-type", "application/json");
                httpURLConnection.setDoInput(true);
                httpURLConnection.connect();


                OutputStream outputStream = httpURLConnection.getOutputStream();
                outputStream.write(postParameters.getBytes("UTF-8"));
                outputStream.flush();
                outputStream.close();


                int responseStatusCode = httpURLConnection.getResponseCode();
                Log.d(TAG, "POST response code - " + responseStatusCode);

                InputStream inputStream;
                if(responseStatusCode == HttpURLConnection.HTTP_OK) {
                    inputStream = httpURLConnection.getInputStream();
                }
                else{
                    inputStream = httpURLConnection.getErrorStream();
                }


                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                StringBuilder sb = new StringBuilder();
                String line = null;

                while((line = bufferedReader.readLine()) != null){
                    sb.append(line);
                }


                bufferedReader.close();


                return sb.toString();


            } catch (Exception e) {

                Log.d(TAG, "InsertData: Error ", e);

                return new String("Error: " + e.getMessage());
            }

        }
    }

    public void setAlarm1( float _density ) {

        long now = System.currentTimeMillis();
        // Data 객체에 시간을 저장한다.
        Date date = new Date(now);
        // 각자 사용할 포맷을 정하고 문자열로 만든다.
        SimpleDateFormat sdfNow = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        String strNow = sdfNow.format(date);

        mNoti = new NotificationCompat.Builder( this.getApplicationContext() )
                .setContentTitle("현재 미세먼지 농도가 위험 수치입니다.")
                .setContentText(String.format("미세먼지 수치 : %f ㎍/㎥", _density))
                .setSmallIcon(R.drawable.dmicon)
                .setTicker("현재 미세먼지 농도가 위험 수치입니다.")
                .build();
        mNoti.defaults |= Notification.DEFAULT_VIBRATE;
        mNoti.vibrate = new long[] {100};
        m_NotiManager.notify(7776, mNoti);

    }
}
