package com.example.dustmonitor;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Arrays;

public class DustMapActivity extends FragmentActivity implements OnMapReadyCallback {

    //SQL DB
    SQLiteDatabase db;
    Cursor cursor;
    Context thisActivity;

    //GoogleMap
    private GoogleMap mMap;
    private ArrayList<Circle> circleList = new ArrayList<Circle>();     //Circle 객체를 담을 ArrayList
    private ArrayList<Marker> markerList = new ArrayList<Marker>();
    public static int REQUEST_CODE_LOCATION = 1;
    private Location lastKnownLocation = null;
    //Default 주소 : 대구시 중심
    private LatLng defaultLatLng = new LatLng(35.871436, 128.601444);
    private ArrayList<LocalLogData> localLogData = new ArrayList<LocalLogData>();

    //Spinner
    Spinner spinner_year;
    Spinner spinner_month;
    Spinner spinner_day;
    ArrayAdapter<String> adapterYear;
    ArrayAdapter<String> adapterMonth;
    ArrayAdapter<String> adapterDay;
    ArrayList<String> array_year = new ArrayList<String>();
    ArrayList<String> array_month = new ArrayList<String>();
    ArrayList<String> array_day = new ArrayList<String>();

    //station Data - 정류장의 번호(이름)과 XY좌표
    String tempstationData[];
    String locationName[];
    String locationX[];
    String locationY[];
    ArrayList<String> stationData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dust_map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        thisActivity = this;
        tempstationData = getResources().getStringArray(R.array.locationNumber);
        locationName = getResources().getStringArray(R.array.locationName);
        locationX = getResources().getStringArray(R.array.locationX);
        locationY = getResources().getStringArray(R.array.locationY);
        stationData = new ArrayList<String>(Arrays.asList(tempstationData));

        //FInd current location. 현재 위치를 찾자
        LocationManager lm = (LocationManager)getSystemService(Context. LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // 사용자 권한 요청
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
            finish();
            return;
        }
        if (lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            lm.requestLocationUpdates( LocationManager.GPS_PROVIDER , 1000, 0, locationListener );
            lastKnownLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        }
        if (lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            lm.requestLocationUpdates( LocationManager.NETWORK_PROVIDER , 1000, 0, locationListener );
            lastKnownLocation = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        lm.removeUpdates( locationListener );    // Stop the update if it is in progress.

        if( lastKnownLocation == null ) {
            lastKnownLocation = new Location("gps");
           // lastKnownLocation.setLatitude(defaultLatLng.latitude);
           // lastKnownLocation.setLongitude(defaultLatLng.longitude);
            //For Test - 현재위치를 경북대학교로
            lastKnownLocation.setLatitude( Float.parseFloat(getResources().getStringArray(R.array.locationX)[0]) );
            lastKnownLocation.setLongitude( Float.parseFloat(getResources().getStringArray(R.array.locationY)[0]) );
        }

        loadDB("*", "");
        if( cursor.getCount() <= 0 ) {
            Toast.makeText( this, "저장된 데이터가 없습니다.", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        setYearSpinner();
        setMonthSpinner();
        setDaySpinner();
    }

    private void setYearSpinner() {

        //Spinner
        spinner_year = (Spinner) findViewById(R.id.dustMap_spinner_year);
        // Create an ArrayAdapter using the string array and a default spinner layout

        loadDB("distinct year", "");
        array_year.clear();
        while ( cursor.moveToNext()) {
            array_year.add(cursor.getString(0));
        }
        closeDB();
        adapterYear = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_year);
        // Specify the layout to use when the list of choices appears
        adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_year.setAdapter(adapterYear);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                setMonthSpinner();
                setDaySpinner();
                clearAllCircle();
                drawCircle();
                moveCameraPositionToLastData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setMonthSpinner() {
        spinner_month = (Spinner) findViewById(R.id.dustMap_spinner_month);
        // Create an ArrayAdapter using the string array and a default spinner layout

        loadDB("distinct month", "where year=" + spinner_year.getSelectedItem().toString() );
        array_month.clear();
        while ( cursor.moveToNext()) {
            array_month.add(cursor.getString(0));
        }
        closeDB();
        adapterMonth = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_month);
        // Specify the layout to use when the list of choices appears
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_month.setAdapter(adapterMonth);
        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                setDaySpinner();
                clearAllCircle();
                drawCircle();
                moveCameraPositionToLastData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setDaySpinner() {

        //Spinner
        spinner_day = (Spinner) findViewById(R.id.dustMap_spinner_day);
        // Create an ArrayAdapter using the string array and a default spinner layout

        loadDB("distinct day", "where year=" + spinner_year.getSelectedItem().toString() + " AND month=" + spinner_month.getSelectedItem().toString());
        array_day.clear();
        while ( cursor.moveToNext()) {
            array_day.add(cursor.getString(0));
        }
        closeDB();
        adapterDay = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, array_day);
        // Specify the layout to use when the list of choices appears
        adapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_day.setAdapter(adapterDay);
        spinner_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                clearAllCircle();
                drawCircle();
                moveCameraPositionToLastData();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadDB(String target, String option) {
        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        cursor = db.rawQuery("SELECT " + target + " FROM dust " + option + " ;", null);
        startManagingCursor(cursor);
    }

    private void closeDB() {
        if(db!=null) {
            db.close();
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if( lastKnownLocation == null ) {
            //위치를 찾을 수 없는 경우 또 앱이 로케이션서비스 권한이 없는 경우.
            // Can't find location && Don't have Permission for LocationService
            // Toast.makeText( this, "위치를 찾을 수 없습니다.", Toast.LENGTH_LONG).show();
            return;
        }

/*        mMap.setOnCircleClickListener(new GoogleMap.OnCircleClickListener() {
            @Override
            public void onCircleClick(Circle circle) {
                AlertDialog.Builder dlg = new AlertDialog.Builder( thisActivity );
                dlg.setTitle( ((LocalLogData) circle.getTag()).getLocationName() + ":" );
                dlg.setMessage( ((LocalLogData) circle.getTag()).toString() );
                dlg.show();

            }
        });*/
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                AlertDialog.Builder dlg = new AlertDialog.Builder( thisActivity );
                dlg.setTitle( ((LocalLogData) marker.getTag()).getLocationName() + ":" );
                dlg.setMessage( ((LocalLogData) marker.getTag()).toString() );
                dlg.show();
                return true;
            }
        });

        moveCameraPositionToLastData();
        drawCircle();
    }

    public void moveCameraPositionToCurrentLocation() {
        mMap.moveCamera( CameraUpdateFactory.newLatLngZoom( new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()), (float) 15.7) );
    }

    //가장 최근의 데이터로 커서를 이동한다.
    public void moveCameraPositionToLastData() {
        int i=0;
        LatLng tempLatlng;
        loadDB("*", "where year=" + spinner_year.getSelectedItem().toString() + " AND month=" + spinner_month.getSelectedItem().toString()
                + " AND day=" + spinner_day.getSelectedItem().toString());
        //마지막 데이터로 이동
        cursor.moveToLast();

        for( String temp : tempstationData ) {
            if (temp.equals(cursor.getString(5)) ) {
                tempLatlng = new LatLng( Float.parseFloat(locationX[i]), Float.parseFloat(locationY[i]) );
                mMap.moveCamera( CameraUpdateFactory.newLatLngZoom( tempLatlng, (float) 15.7) );
                closeDB();
                return;
            }
            i++;
        }
        closeDB();
        //만약 데이터를 찾지 못한다면 현재위치로
        moveCameraPositionToCurrentLocation();
    }

    private LocationListener locationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            LocationManager lm = (LocationManager)getSystemService(Context. LOCATION_SERVICE);

            // Get the last location, and update UI.
            lastKnownLocation = location;
            // Stop the update to prevent changing the location.
            lm.removeUpdates( this );
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}

    };

    //DrawCircle
    public void drawCircle() {
        int checkExistLocationData = -1;
        float tempfloat;
        int tempint;
        LocalLogData temp_locallogdata;



        loadDB("*", "where year=" + spinner_year.getSelectedItem().toString() + " AND month=" + spinner_month.getSelectedItem().toString()
                + " AND day=" + spinner_day.getSelectedItem().toString());

        while ( cursor.moveToNext() ) {
            //localLogData 배열에 이미 존재하는 위치에 관한 데이터면, 거기에 추가하고
            //존재하지 않는 위치에 관한 데이터면 새롭게 추가한다.
            checkExistLocationData = -1;
            for(int i=0; i<localLogData.size(); i++) {
                //찾고자 하는 데이터가 이미 있으면
                if( (localLogData.get(i).getCurrentLocation()).equals(cursor.getString(5)) ) {
                    localLogData.get(i).addLogdata( new Logdata(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getString(5), cursor.getDouble(6)) );
                    checkExistLocationData = 1;
                    break;
                }
            }

            if(checkExistLocationData == 1) continue;

            temp_locallogdata = new LocalLogData( cursor.getString(5).replaceAll(" ", ""), locationName[stationData.indexOf( cursor.getString(5).replaceAll(" ", "") )] );
            temp_locallogdata.addLogdata( new Logdata(cursor.getInt(1), cursor.getInt(2), cursor.getInt(3), cursor.getInt(4), cursor.getString(5), cursor.getDouble(6)) );
            localLogData.add( temp_locallogdata );
        }
        closeDB();

        for(int i=0; i < localLogData.size(); i++) {
            //System.out.printf("%s", localLogData.get(i).toString());
            System.out.println(localLogData.get(i).currentLocation + ": " + localLogData.get(i).getAverageDensitiy() );
            tempint = stationData.indexOf( localLogData.get(i).currentLocation );
            if (tempint == -1) continue;
            CircleOptions circleOptions = new CircleOptions()
                    .center(new LatLng( Float.parseFloat(locationX[tempint]), Float.parseFloat(locationY[tempint]) ) )
                    .radius(70) // In meters
                    .strokeWidth(1)
                    .strokeColor(R.color.colorTransparent)
                    .fillColor(Color.argb(128, 255, 0, 0))
                    ;

            tempfloat = localLogData.get(i).getAverageDensitiy();

            //농도 : 좋음
            if ( tempfloat > 0 && tempfloat < 50 )
            {
                circleOptions.fillColor( Color.argb(128, 20, 137, 204));
            }
            //농도 : 보통
            else if ( tempfloat >= 50 && tempfloat < 100 ) {
                circleOptions.fillColor( Color.argb(128, 3, 129, 143));
            }
            //농도 : 나쁨
            else if ( tempfloat >= 100 && tempfloat < 250 ) {
                circleOptions.fillColor( Color.argb(128, 255, 117, 18));
            }
            //농도 : 매우 나쁨
            else if ( tempfloat >= 250 ) {
                circleOptions.fillColor( Color.argb(128, 230, 71, 39));
            }
            Circle circle = mMap.addCircle(circleOptions);
          /*  circle.setClickable(true);
            circle.setTag( localLogData.get(i) );*/

            circleList.add(circle);

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position( new LatLng( Float.parseFloat(locationX[tempint]), Float.parseFloat(locationY[tempint]) ) )
                    .title(localLogData.get(i).getLocationName()));
            marker.setTag( localLogData.get(i) );
            markerList.add(marker);
        }

    }

    private void clearAllCircle() {
        int i;

        for(i=0; i<circleList.size();i++) {
            circleList.get(i).remove();
            markerList.get(i).remove();
        }
        circleList.clear();
        localLogData.clear();
        markerList.clear();
    }

    private class LocalLogData {
        private String locationName;
        private String currentLocation;
        private ArrayList<Logdata> log = new ArrayList<Logdata>();

        public LocalLogData (String currentlocation, String locationname) {
            currentLocation = currentlocation;
            locationName = locationname;
        }

        public void addLogdata(Logdata logdata) {
            log.add(logdata);
        }

        public int getCount() {
            return log.size();
        }

        public float getAverageDensitiy() {
            float result = 0;
            if( log.size() <= 0 ) return result;
            for ( Logdata temp : log ) {
                result += temp.getDensity();
            }
            result /= log.size();
            return result;
        }

        public String getCurrentLocation() {
            return currentLocation;
        }

        public String getLocationName() {
            return locationName;
        }
        @Override
        public String toString() {
            String result = "";
            String temp;

            result = result + currentLocation + " :\n";
            for( int i=0; i < log.size(); i++ ) {
                temp = String.format("%4d/%2d/%2d/%2s:%2s : %.2f", log.get(i).getYear(), log.get(i).getMonth(), log.get(i).getDay(),
                        String.format("%04d", log.get(i).getTime()).substring(0,2), String.format("%04d", log.get(i).getTime()).substring(2), log.get(i).getDensity() );
                result = result + temp + "\n";
            }

            return result;
        }
    }

}
