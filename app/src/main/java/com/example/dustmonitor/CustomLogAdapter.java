package com.example.dustmonitor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 하품쟁이 on 2017-11-07.
 */

public class CustomLogAdapter extends BaseAdapter {

    private ArrayList<Logdata> listViewItemList = new ArrayList<Logdata>();

    public CustomLogAdapter () {


    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        final int pos = position;
        final Context context =  parent.getContext();

        // view == null이면 "listview_item" Layout을 inflate하여 참조 획득.
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_log, parent, false);
        }

        TextView dateTexView = (TextView) view.findViewById(R.id.log_date);
        TextView locationTexView = (TextView) view.findViewById(R.id.log_location);
        TextView densityTexView = (TextView) view.findViewById(R.id.log_density);

        Logdata currentItem = listViewItemList.get(pos);

        dateTexView.setText( String.format("%4d/%02d/%02d/%02d:%02d", currentItem.getYear(), currentItem.getMonth(), currentItem.getDay(),
                currentItem.getTime()/100, currentItem.getTime()%100) );
        /*
        dateTexView.setText(Integer.toString(currentItem.getYear()) + "/" + Integer.toString(currentItem.getMonth()) + "/" + Integer.toString(currentItem.getDay()) + "/"
                + Integer.toString(currentItem.getTime()/100) + ":" + Integer.toString(currentItem.getTime()%100) );
        */
        locationTexView.setText(currentItem.getLocation());
        densityTexView.setText( Double.toString(currentItem.getDensity()) );


        return view;
    }

    @Override
    public int getCount() {
        return listViewItemList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return listViewItemList.get(position);
    }

    public void addItem( int year, int month, int day, int time, String location, double density ) {
        Logdata temp = new Logdata();
        temp.setYear(year);
        temp.setMonth(month);
        temp.setDay(day);
        temp.setTime(time);
        temp.setLocation(location);
        temp.setDensity(density);

        listViewItemList.add(temp);
    }

    public void clearList() {
        listViewItemList.clear();
    }

}
