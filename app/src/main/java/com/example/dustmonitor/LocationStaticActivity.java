package com.example.dustmonitor;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class LocationStaticActivity extends AppCompatActivity {
    Spinner spinner_year;
    Spinner spinner_month;
    Spinner spinner_day;
    ArrayAdapter<String> adapterYear;
    ArrayAdapter<String> adapterMonth;
    ArrayAdapter<String> adapterDay;
    ArrayList<String> array_year = new ArrayList<String>();
    ArrayList<String> array_month = new ArrayList<String>();
    ArrayList<String> array_day = new ArrayList<String>();

    int current_year;
    int current_day;
    int current_month;

    BarChart chart ;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels ;
    BarDataSet Bardataset ;
    BarData BARDATA ;
    BarChart chart1 ;
    ArrayList<BarEntry> BARENTRY1 ;
    ArrayList<String> BarEntryLabels1 ;
    BarDataSet Bardataset1 ;
    BarData BARDATA1 ;
    //ArrayList<> color;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_static);
        //mpchart
        chart = (BarChart) findViewById(R.id.chart);
        chart1 = (BarChart) findViewById(R.id.chart1);
        //int color = getResources().getColor(R.color.bad);
        BARENTRY = new ArrayList<>();

        BarEntryLabels = new ArrayList<String>();


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        //뒤에 격자 지우기
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawLabels(true);
        ///
        AddValuesToBARENTRY();

        AddValuesToBarEntryLabels();

        Bardataset = new BarDataSet(BARENTRY, "");

        BARDATA   = new BarData(BarEntryLabels, Bardataset);

        Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);


        chart.setData(BARDATA);
        chart.setDoubleTapToZoomEnabled(false);
        xAxis.setSpaceBetweenLabels(0);
        chart.setVisibleXRangeMaximum(6);
        chart.setDrawValueAboveBar(true);
        chart.animateXY(1000, 1000);

        // Bardataset.setColor(color);
    /*
        for(int i=0; i<BARENTRY.size(); i++)
        {
            //System.out.println("hhhhhhhhiiiiiiiiiiiiiiiii"+BARENTRY.get(i).getVal());
            if(BARENTRY.get(i).getVal()>1)
            {
                Bardataset.setColor(R.color.bad);
            }
            else
                Bardataset.setColor(R.color.bad);


        }*/
        ////start mpchart1



        BARENTRY1 = new ArrayList<>();

        BarEntryLabels1 = new ArrayList<String>();

        XAxis xAxis1 = chart1.getXAxis();
        xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
        AddValuesToBARENTRY1();
        AddValuesToBarEntryLabels1();

        Bardataset1 = new BarDataSet(BARENTRY1, "");

        BARDATA1   = new BarData(BarEntryLabels1, Bardataset1);

        Bardataset1.setColors(ColorTemplate.COLORFUL_COLORS);


        chart1.setData(BARDATA1);
        chart1.setDoubleTapToZoomEnabled(false);
        xAxis1.setSpaceBetweenLabels(1);
        chart1.setVisibleXRangeMaximum(10);
        chart1.setDrawValueAboveBar(true);
        chart1.animateXY(1000, 1000);


        //Spinner Year
        spinner_year = (Spinner) findViewById(R.id.PersonalStatic_spinner_year);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterYear = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_year);
        // Specify the layout to use when the list of choices appears
        adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_year.setAdapter(adapterYear);

        //Spinner Month
        spinner_month = (Spinner) findViewById(R.id.PersonalStatic_spinner_month);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterMonth = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_month);
        // Specify the layout to use when the list of choices appears
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_month.setAdapter(adapterMonth);

        spinner_day = (Spinner) findViewById(R.id.PersonalStatic_spinner_day);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterDay = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_day);
        // Specify the layout to use when the list of choices appears
        adapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_day.setAdapter(adapterDay);

       // getFindDust("옥천동");
    }

    public static void getFindDust(String name){	//대기정보를 가져오는 스레드

        GetFindDustThread.active=true;
        GetFindDustThread getweatherthread=new GetFindDustThread(false,name);		//스레드생성(UI 스레드사용시 system 뻗는다)
        getweatherthread.start();	//스레드 시작

    }

    public void AddValuesToBARENTRY(){

        BARENTRY.add(new BarEntry(2f, 0));
        BARENTRY.add(new BarEntry(4f, 1));
        BARENTRY.add(new BarEntry(6f, 2));
        BARENTRY.add(new BarEntry(8f, 3));
        BARENTRY.add(new BarEntry(7f, 4));
        BARENTRY.add(new BarEntry(23f, 5));

    }

    public void AddValuesToBarEntryLabels(){

        BarEntryLabels.add("January");
        BarEntryLabels.add("February");
        BarEntryLabels.add("March");
        BarEntryLabels.add("April");
        BarEntryLabels.add("May");
        BarEntryLabels.add("June");
        BarEntryLabels.add("July");
        BarEntryLabels.add("August");
        BarEntryLabels.add("September");
        BarEntryLabels.add("October");
        BarEntryLabels.add("November");
        BarEntryLabels.add("December");



    }

    public void AddValuesToBARENTRY1(){

        BARENTRY1.add(new BarEntry(2f, 0));
        BARENTRY1.add(new BarEntry(4f, 1));
        BARENTRY1.add(new BarEntry(6f, 2));
        BARENTRY1.add(new BarEntry(8f, 3));
        BARENTRY1.add(new BarEntry(7f, 4));
        BARENTRY1.add(new BarEntry(3f, 5));
        BARENTRY1.add(new BarEntry(2f, 6));
        BARENTRY1.add(new BarEntry(4f, 7));
        BARENTRY1.add(new BarEntry(6f, 8));
        BARENTRY1.add(new BarEntry(8f, 9));
        BARENTRY1.add(new BarEntry(7f, 10));
        BARENTRY1.add(new BarEntry(23f, 11));


    }

    public void AddValuesToBarEntryLabels1(){
        int i;
        for(i=0; i<=30; i++)
        {
            BarEntryLabels1.add(Integer.toString(i));

        }


    }

}
