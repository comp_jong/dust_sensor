package com.example.dustmonitor;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import java.util.ArrayList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class PersonalStaticActivity extends AppCompatActivity {
    //DB Variable
    SQLiteDatabase db;
    Cursor cursor;

    Spinner spinner_year;
    Spinner spinner_month;
    Spinner spinner_day;
    ArrayAdapter<String> adapterYear;
    ArrayAdapter<String> adapterMonth;
    ArrayAdapter<String> adapterDay;
    ArrayList<String> array_year = new ArrayList<String>();
    ArrayList<String> array_month = new ArrayList<String>();
    ArrayList<String> array_day = new ArrayList<String>();

    int current_year;
    int current_day;
    int current_month;

    BarChart chart ;
    ArrayList<BarEntry> BARENTRY ;
    ArrayList<String> BarEntryLabels ;
    BarDataSet Bardataset ;
    BarData BARDATA ;
    BarChart chart1 ;
    ArrayList<BarEntry> BARENTRY1 ;
    ArrayList<String> BarEntryLabels1 ;
    BarDataSet Bardataset1 ;
    BarData BARDATA1 ;
    //ArrayList<> color;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_static);
        //mpchart
        loadDB("*","");
        if(cursor.getCount()<=0){
            return ;
        }

        closeDB();
        setYearSpinner();
        setMonthSpinner();
        setDaySpinner();

        drawChart();

    }

    private void drawChart() {

        chart = (BarChart) findViewById(R.id.chart);
        chart1 = (BarChart) findViewById(R.id.chart1);
        //int color = getResources().getColor(R.color.bad);
        BARENTRY = new ArrayList<>();

        BarEntryLabels = new ArrayList<String>();


        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis leftAxis = chart.getAxisLeft();
        YAxis rightAxis = chart.getAxisRight();
        //뒤에 격자 지우기
        xAxis.setDrawAxisLine(false);
        xAxis.setDrawGridLines(false);
        leftAxis.setDrawLabels(false);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawLabels(true);
        ///
        AddValuesToBARENTRY();

        AddValuesToBarEntryLabels();

        Bardataset = new BarDataSet(BARENTRY, "");

        BARDATA   = new BarData(BarEntryLabels, Bardataset);

        Bardataset.setColors(ColorTemplate.COLORFUL_COLORS);


        chart.setData(BARDATA);
        chart.setDoubleTapToZoomEnabled(false);
        xAxis.setSpaceBetweenLabels(0);
        chart.setVisibleXRangeMaximum(6);
        chart.setDrawValueAboveBar(true);
        chart.animateXY(1000, 1000);

        Float tempfloat = Float.parseFloat(spinner_day.getSelectedItem().toString());
        tempfloat -= 4;
        if ( tempfloat > 0  ) {
            chart.moveViewToX( tempfloat );
        }

        // Bardataset.setColor(color);
    /*
        for(int i=0; i<BARENTRY.size(); i++)
        {
            //System.out.println("hhhhhhhhiiiiiiiiiiiiiiiii"+BARENTRY.get(i).getVal());
            if(BARENTRY.get(i).getVal()>1)
            {
                Bardataset.setColor(R.color.bad);
            }
            else
                Bardataset.setColor(R.color.bad);


        }*/
        ////start mpchart1



        BARENTRY1 = new ArrayList<>();

        BarEntryLabels1 = new ArrayList<String>();

        XAxis xAxis1 = chart1.getXAxis();
        xAxis1.setPosition(XAxis.XAxisPosition.BOTTOM);
        AddValuesToBARENTRY1();
        AddValuesToBarEntryLabels1();

        Bardataset1 = new BarDataSet(BARENTRY1, "");

        BARDATA1   = new BarData(BarEntryLabels1, Bardataset1);

        Bardataset1.setColors(ColorTemplate.COLORFUL_COLORS);


        chart1.setData(BARDATA1);
        chart1.setDoubleTapToZoomEnabled(false);
        xAxis1.setSpaceBetweenLabels(1);
        chart1.setVisibleXRangeMaximum(10);
        chart1.setDrawValueAboveBar(true);
        chart1.animateXY(1000, 1000);

    }

    private void setYearSpinner() {
        //Spinner Year

        loadDB("distinct year", "");

        array_year.clear();
        while ( cursor.moveToNext() ) {
            array_year.add( cursor.getString(0) );
        }

        spinner_year = (Spinner) findViewById(R.id.PersonalStatic_spinner_year);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterYear = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_year);
        // Specify the layout to use when the list of choices appears
        adapterYear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_year.setAdapter(adapterYear);
        spinner_year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                setMonthSpinner();
                setDaySpinner();
                drawChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        closeDB();
    }

    private void setMonthSpinner() {

//        System.out.println(spinner_year.getSelectedItem().toString());



        loadDB("DISTINCT month", "where year=" + spinner_year.getSelectedItem().toString() );

        array_month.clear();
        while ( cursor.moveToNext() ) {
            array_month.add( cursor.getString(0) );
        }

        //Spinner Month
        spinner_month = (Spinner) findViewById(R.id.PersonalStatic_spinner_month);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterMonth = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_month);
        // Specify the layout to use when the list of choices appears
        adapterMonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_month.setAdapter(adapterMonth);
        spinner_month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                setDaySpinner();
                drawChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        closeDB();
    }

    private void setDaySpinner() {


        loadDB("distinct day", "where year=" + spinner_year.getSelectedItem().toString() + " AND month=" + spinner_month.getSelectedItem().toString());

        array_day.clear();
        while ( cursor.moveToNext() ) {
            array_day.add( cursor.getString(0) );
        }

        spinner_day = (Spinner) findViewById(R.id.PersonalStatic_spinner_day);
        // Create an ArrayAdapter using the string array and a default spinner layout
        adapterDay = new ArrayAdapter<String> (this,
                android.R.layout.simple_spinner_item, array_day);
        // Specify the layout to use when the list of choices appears
        adapterDay.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner_day.setAdapter(adapterDay);
        spinner_day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //각 항목 클릭시 포지션값을 토스트에 띄운다.
                drawChart();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        closeDB();
    }

    public void AddValuesToBARENTRY(){

        loadDB("*", "where year="+ spinner_year.getSelectedItem().toString() + " AND Month=" + spinner_month.getSelectedItem().toString() + " order by day");

        float sum = 0;  //density SUM
        int currentday=1;
        int count = 0;

        System.out.println(currentday);

        while (cursor.moveToNext()) {
            if (currentday != (((int) cursor.getInt(3)))) {
                if(count == 0) count = 1;
                BARENTRY.add(new BarEntry(sum/count, currentday-1));
                currentday = (((int) cursor.getInt(3)));
                sum = 0;
                count = 0;
                System.out.println(currentday);
            }
            count++;
            sum += cursor.getInt(6);
        }

        BARENTRY.add(new BarEntry(sum/count, currentday-1));
        System.out.println(currentday);


        closeDB();

    }

    public void AddValuesToBarEntryLabels(){

        int i;
        for(i=1; i<=31; i++)
        {
            BarEntryLabels.add(Integer.toString(i));

        }





    }

    private void loadDB(String target, String option) {
        db = openOrCreateDatabase(
                "test.db",
                SQLiteDatabase.CREATE_IF_NECESSARY,
                null
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS dust " +
                "(_id INTEGER PRIMARY KEY AUTOINCREMENT, year INTEGER, month INTEGER, day INTEGER, time INTEGER, location TEXT, density REAL);");

        cursor = db.rawQuery("SELECT " + target + " FROM dust " + option + " ;", null);
        startManagingCursor(cursor);
    }

    private void closeDB() {
        if(db!=null) {
            db.close();
        }
    }

    public void AddValuesToBARENTRY1(){

        loadDB("*", "where year=" + spinner_year.getSelectedItem().toString() + " AND Month=" + spinner_month.getSelectedItem().toString() + " AND day=" + spinner_day.getSelectedItem().toString()
                + " order by time");

        float sum = 0;  //density SUM
        int currenttime=0;
        int count = 0;

        System.out.println(currenttime);

        while (cursor.moveToNext()) {
            if (currenttime != (((int) cursor.getInt(4)) / 100 * 100)) {
                if(count == 0) count = 1;
                BARENTRY1.add(new BarEntry(sum/count, currenttime/100));
                currenttime = (((int) cursor.getInt(4)) / 100 * 100);
                sum = 0;
                count = 0;
                System.out.println(currenttime);
            }
            count++;
            sum += cursor.getInt(6);
        }

        BARENTRY1.add(new BarEntry(sum/count, currenttime/100));
        System.out.println(currenttime);


        closeDB();
    }

    public void AddValuesToBarEntryLabels1(){
        int i;
        for(i=0; i<=24; i++)
        {
            BarEntryLabels1.add(Integer.toString(i));

        }


    }
}
